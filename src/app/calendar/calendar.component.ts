import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import {
  CalendarEvent,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';

import {CalendarService} from './calendar.service';

@Component({
  selector: 'app-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  view = 'month';

  constructor(
    private calendarService: CalendarService
  ) {
  }

  viewDate: Date = new Date();


  events: CalendarEvent[] = [];

  refresh: Subject<any> = new Subject();

  ngOnInit() {
    this.observeCalendarData();
  }

  private observeCalendarData() {
    this.calendarService.observeCalendarData()
      .subscribe( (data) => {
        this.events = data;
        this.refresh.next();
      } );
  }

  addEvent(date: Date): void {
    const eventName = prompt('Please enter event name', '');
    this.calendarService.addEvent({
      start: date,
      title: eventName,
      color: this.calendarService.colors.red,
      draggable: true
    });
  }

  removeEvent(date: Date): void {
    this.calendarService.removeEvent(date);
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.refresh.next();
  }

}
