import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  private trackCalendarData = new Subject<any>();
  colors: any = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3'
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF'
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA'
    }
  };

  private calendarData = [
  ];

  constructor() {
    this.publishEvents();
  }

  private publishEvents() {
    this.trackCalendarData.next(this.calendarData);
  }

  addEvent(eventItem) {
    this.calendarData.push(eventItem);
    this.publishEvents();
  }

  removeEvent (eventItem) {
    const index = this.calendarData.findIndex(item => item.title === eventItem.title);
    this.calendarData.splice(index, 1);
    this.publishEvents();
  }

  observeCalendarData() {
    return this.trackCalendarData.asObservable();
  }

}
